/* 
 * File:   imove.h
 * Author: hokum
 *
 * Created on January 31, 2016, 11:09 AM
 */

#ifndef IMOVE_H
#define	IMOVE_H

#include <vector>
#include <string>
#include <memory>

typedef std::vector<unsigned> state;

class imove
{
public:
    virtual state operator()(const state& cur) const = 0;
    virtual std::unique_ptr<imove> clone() const = 0;
    virtual std::string to_string() const = 0;
    virtual ~imove() {}
};

typedef std::shared_ptr<const imove> imove_ptr;

#endif	/* IMOVE_H */

