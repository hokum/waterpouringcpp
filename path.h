/* 
 * File:   path.h
 * Author: hokum
 *
 * Created on January 31, 2016, 11:26 AM
 */

#ifndef PATH_H
#define	PATH_H

#include "imove.h"

#include <list>

std::string to_string(const state& st);

class path
{
public:
    path(const state& initial_state);
    path(const path& that);

    void extend(imove_ptr move);
    const state& end_state() const;
    std::string to_string() const;
    bool empty() const;
private:
    std::list<imove_ptr> _history;
    state _end_state;
};

#endif	/* PATH_H */

