includes = .

target = WaterPouring

INCLUDES = $(addprefix -I, $(addprefix $parentdir,$(includes)))

CXXFLAGS=-g -std=c++14 -Wall -Wpedantic -Wextra $(INCLUDES)

outputdir = bin

.PHONY: clean all

ifndef parentdir

export sources = $(wildcard *.cpp)

export objects = $(patsubst %.cpp,%.o,$(sources))

export parentdir=..

.PHONY: $(target)

$(target):
	mkdir -p $(outputdir)
	mkdir -p $(addprefix $(outputdir)/, $(dir $(objects)))
	$(MAKE) --directory=$(outputdir) --makefile=$(parentdir)/Makefile $(target)

all:
	mkdir -p $(outputdir)
	mkdir -p $(addprefix $(outputdir)/, $(dir $(objects)))
	$(MAKE) --directory=$(outputdir) --makefile=$(parentdir)/Makefile all

else

VPATH = $(parentdir)

$(target): lazy $(objects)
	$(CXX) $(objects) -o $(target)

lazy:
	git clone https://hokum@bitbucket.org/hokum/lazy_list.git $(parentdir)/lazy

all: $(target)

endif

clean:
	rm -rf $(outputdir)

