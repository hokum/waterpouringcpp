/* 
 * File:   water_pouring.cpp
 * Author: hokum
 * 
 * Created on January 30, 2016, 6:34 PM
 */

#include "water_pouring.h"

#include <algorithm>
#include <iostream>

namespace
{

class fill: public imove
{
public:
    fill(unsigned glass, unsigned capacity);
    state operator()(const state& cur) const override;
    std::unique_ptr<imove> clone() const override;
    std::string to_string() const override;
protected:
    const unsigned _glass;
    const unsigned _capacity;
};

fill::fill(unsigned glass, unsigned capacity) :
    _glass(glass),
    _capacity(capacity)
{}

state fill::operator()(const state& cur) const
{
    assert(cur.size() > _glass);
    state next(cur);
    next[_glass] = _capacity;
    return next;
}
std::unique_ptr<imove> fill::clone() const
{
    return std::unique_ptr<imove>(new fill(_glass, _capacity));
}
std::string fill::to_string() const
{
    return "fill(" + std::to_string(_glass) + ")";
}
///////////////////////////////////////////////////////////////////////////////

class empty: public fill
{
public:
    empty(unsigned glass);
    std::unique_ptr<imove> clone() const override;
    std::string to_string() const override;
};

empty::empty(unsigned glass) :
    fill(glass, 0)
{}

std::unique_ptr<imove> empty::clone() const
{
    return std::unique_ptr<imove>(new empty(_glass));
}

std::string empty::to_string() const
{
    return "empty(" + std::to_string(_glass) + ")";
}

///////////////////////////////////////////////////////////////////////////////

class pour: public imove
{
public:
    pour(unsigned from, unsigned to, unsigned capacity_to);
    state operator()(const state& cur) const override;
    std::unique_ptr<imove> clone() const override;
    std::string to_string() const override;
protected:
    const unsigned _from;
    const unsigned _to;
    const unsigned _capacity_to;
};

pour::pour(unsigned from, unsigned to, unsigned capacity_to) :
    _from(from),
    _to(to),
    _capacity_to(capacity_to)
{}

state pour::operator()(const state& cur) const
{
    assert((cur.size() > _from) && (cur.size() > _to));
    assert(_capacity_to >= cur[_to]);
    unsigned amount = std::min(cur[_from], _capacity_to - cur[_to]);
    state next(cur);
    next[_from] -= amount;
    next[_to] += amount;
    return next;
}

std::unique_ptr<imove> pour::clone() const
{
    return std::unique_ptr<imove>(new pour(_from, _to, _capacity_to));
}

std::string pour::to_string() const
{
    return "pour(" + std::to_string(_from) + ", " + std::to_string(_to) + ")";
}

//////////////////////////////////////////////////////////////////////////////

std::vector<imove_ptr> create_moves(const std::vector<unsigned>& capacities)
{
    std::vector<imove_ptr> moves;
    for (size_t i = 0; i < capacities.size(); ++i)
    {
        moves.emplace_back(new empty(i));
        moves.emplace_back(new fill(i, capacities[i]));
        for (size_t j = 0; j < capacities.size(); ++j)
        {
            if (i != j)
                moves.emplace_back(new pour(i, j, capacities[j]));
        }
    }
    return moves;
}

} /*anonymouse namespace*/

//////////////////////////////////////////////////////////////////////////////

water_pouring::water_pouring(std::initializer_list<unsigned> capacities) :
    _capacities(capacities),
    _posible_moves(create_moves(_capacities)),
    _initial(capacities.size())
{
    _explored_states.insert(_initial);
    paths_list initial_paths = { path(_initial) };
    _paths.push_back(extend(initial_paths));
}

path water_pouring::solve(unsigned target)
{
    paths_list::const_iterator solution;
    auto it = std::find_if(
        _paths.begin(),
        _paths.end(),
        [target, &solution](const paths_list& paths) -> bool {
            solution = std::find_if(
                paths.begin(),
                paths.end(),
                [target](const path& p) -> bool {
                    auto it = std::find(
                            p.end_state().begin(),
                            p.end_state().end(),
                            target);
                    return it != p.end_state().end();
                });
            return solution != paths.end();
        });

    if (it != _paths.end())
        return *solution;

    return path(state({0}));
}

water_pouring::list_of_paths_type water_pouring::extend(const paths_list& paths)
{
    paths_list next;
    for (auto& cur_path: paths)
    {
        for (auto move: _posible_moves)
        {
            state next_state = (*move)(cur_path.end_state());

            if (_explored_states.find(next_state) == _explored_states.end())
            {
                path new_path(cur_path);
                new_path.extend(move);
                next.push_back(new_path);
                _explored_states.insert(next_state);
            }
        }
    }

    if (next.empty())
        return list_of_paths_type();

    return list_of_paths_type(next, std::bind(&water_pouring::extend, this, next));
}
