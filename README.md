# README #

Решение задачи water pouring с использованием [lazy_list](https://bitbucket.org/hokum/lazy_list).

Для сборки необходимо получить lazy_list выполнив в корне репозитория команду:

```
git clone https://hokum@bitbucket.org/hokum/lazy_list.git lazy
```

В линуксе можно просто выполнить комманду make. Исходный код lazy_list будет скачан автоматически.