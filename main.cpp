/* 
 * File:   main.cpp
 * Author: hokum
 *
 * Created on January 28, 2016, 11:39 PM
 */

#include "water_pouring.h"

#include <iostream>
#include <chrono>

using namespace std;

int main(int /*argc*/, char** /*argv*/)
{
    water_pouring problem({3, 5});
    unsigned target = 4;

    {
        auto start = std::chrono::steady_clock::now();
        auto solution = problem.solve(target);
        auto stop = std::chrono::steady_clock::now();
        auto res = stop - start;
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(res).count() << "ms\n";

        if (!solution.empty())
            cout << solution.to_string() << endl;
        else
            cout << "solution was not found" << endl;
    }

    {
        auto start = std::chrono::steady_clock::now();
        auto solution = problem.solve(target);
        auto stop = std::chrono::steady_clock::now();
        auto res = stop - start;
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(res).count() << "ms\n";

        if (!solution.empty())
            cout << solution.to_string() << endl;
        else
            cout << "solution was not found" << endl;
    }

    return 0;
}

