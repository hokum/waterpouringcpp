/* 
 * File:   water_pouring.h
 * Author: hokum
 *
 * Created on January 30, 2016, 6:34 PM
 */

#ifndef WATER_POURING_H
#define	WATER_POURING_H

#include "imove.h"
#include "path.h"

#include "lazy/lazy_list.h"

#include <utility>
#include <set>

typedef std::list<path> paths_list;

class water_pouring
{
public:
    water_pouring(std::initializer_list<unsigned> capacities);
    
    path solve(unsigned target);
private:
    typedef lazy::list<paths_list> list_of_paths_type;
    list_of_paths_type extend(const paths_list& paths);

    const std::vector<unsigned> _capacities;
    const std::vector<imove_ptr> _posible_moves;
    const state _initial;
    std::set<state> _explored_states;
    list_of_paths_type _paths;
};

#endif	/* WATER_POURING_H */

