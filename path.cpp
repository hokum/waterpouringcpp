/* 
 * File:   path.cpp
 * Author: hokum
 * 
 * Created on January 31, 2016, 11:26 AM
 */

#include "path.h"

std::string to_string(const state& st)
{
    std::string out;
    for(auto it: st)
    {
        if(!out.empty())
            out += ", ";
        out += std::to_string(it);
    }
    return "(" + out + ")";
}

/////////////////////////////////////////////////////////////////////////////

path::path(const state& initial_state) :
    _end_state(initial_state)
{}

path::path(const path& that) :
    _history(that._history),
    _end_state(that.end_state())
{}

void path::extend(imove_ptr move)
{
    _history.push_back(move);
    _end_state = (*move)(_end_state);
}

const state& path::end_state() const
{
    return _end_state;
}

std::string path::to_string() const
{
    std::string out;
    for(auto move: _history)
        out += move->to_string() + " ";

    return out + "--> " + ::to_string(_end_state);
}

bool path::empty() const
{
    return _history.empty();
}

